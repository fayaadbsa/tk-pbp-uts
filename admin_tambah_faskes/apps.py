from django.apps import AppConfig


class AdminTambahFaskesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'admin_tambah_faskes'
