from django.db import models

# Create your models here.
class Lokasi(models.Model):
    name=models.CharField(max_length=100)
    kuota = models.IntegerField(blank=True, null=True)
    def __str__(self):
        return self.name
        
class TanggalWaktu(models.Model):
    name = models.CharField(max_length=100)
    lokasi=models.ForeignKey(Lokasi,on_delete=models.CASCADE, related_name='tanggal')
    def __str__(self):
        return self.name
class JenisVaksin(models.Model):
    name=models.CharField(max_length=100)
    tanggal=models.ForeignKey(TanggalWaktu,on_delete=models.CASCADE, related_name='jenis')
    def __str__(self):
        return self.name