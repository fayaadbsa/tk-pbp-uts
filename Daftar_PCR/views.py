from django.shortcuts import render,redirect
from .forms import dataForm
from .models import data_PCR

# Create your views here.

def daftar_awal(request):
    form = dataForm()

    if request.method == 'POST':
        form = dataForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/user/afterForm/')

    context = {'form':form}
    return render(request, 'daftar_awal.html', context)

def daftar_akhir(request):
    dataDiri = data_PCR.objects.all().values()
    response = {'dataDiri' : dataDiri}
    return render(request, 'daftar_akhir.html', response)
