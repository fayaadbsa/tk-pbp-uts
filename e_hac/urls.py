from django.urls import path
from django.urls.resolvers import URLPattern
from .views import add_ehac, ehac_list

urlpatterns = [
    path('add-ehac', add_ehac, name='add_ehac'),
    path('ehac-list', ehac_list, name='ehac_list')
]