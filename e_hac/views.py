from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from .models import EHac
from .forms import EHacForm

# Create your views here.

def add_ehac(request):
    form = EHacForm()

    if request.method == 'POST':
        form = EHacForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('ehac-list')

    response = {'form' : form} 
    return render(request, 'ehac_form.html',response)

def ehac_list(request):
    ehac = EHac.objects.all().values()
    response = {'ehac' : ehac}

    return render(request, 'ehac_list.html', response)