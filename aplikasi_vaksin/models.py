from django.db import models
from admin_tambah_faskes.models import Lokasi, TanggalWaktu, JenisVaksin
# Create your models here.
class User(models.Model):
    email = models.EmailField()
    password = models.CharField(max_length=30)
    name = models.CharField(max_length=30)
    no_hp = models.CharField(max_length=12)
    usia = models.CharField(max_length=3)
    nik = models.CharField(max_length=15)
    domisili = models.CharField(max_length=50)
    lokasi = models.ForeignKey(Lokasi, on_delete=models.SET_NULL, blank=True, null=True)
    tanggal = models.ForeignKey(TanggalWaktu, on_delete=models.SET_NULL, blank=True, null=True)
    jenis = models.ForeignKey(JenisVaksin, on_delete=models.SET_NULL, blank=True, null=True)
    def __str__(self) -> str:
        return f"{self.name} | {self.email}"
