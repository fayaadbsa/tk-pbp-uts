from django.shortcuts import render
from aplikasi_vaksin.models import User

# Create your views here.
def index(request):
    users = User.objects.all().values()
    print('user now', request.user)
    name_user = ''
    for user in users:
        if user['email'] == str(request.user):
            name_user = user['name']
            break
    if name_user != '':
        if len(name_user) > 10:
            if len(name_user.split()[0]) > 10:
                name_user = name_user.split()[0][:11] + "..."
            else:
                name_user = name_user.split()[0]
    return render(request, 'home/index.html', {'name': name_user})