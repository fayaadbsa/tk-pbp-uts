# Generated by Django 3.2.7 on 2021-11-04 23:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('admin_tambah_faskes', '0002_auto_20211105_0634'),
        ('aplikasi_vaksin', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='jenis',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='admin_tambah_faskes.jenisvaksin'),
        ),
        migrations.AddField(
            model_name='user',
            name='lokasi',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='admin_tambah_faskes.lokasi'),
        ),
        migrations.AddField(
            model_name='user',
            name='tanggal',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='admin_tambah_faskes.tanggalwaktu'),
        ),
    ]
