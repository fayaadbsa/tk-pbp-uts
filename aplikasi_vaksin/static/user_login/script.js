
const emails = document.getElementsByClassName('emails');
const passwords = document.getElementsByClassName('passwords');
function email_is_valid(){
  for (let x in json) {
    if ($('#id_email').val() == json[x]['email']) return true;
    // console.log(users_as_json[user]['email']);
    // console.log('email: ' + user['email'] + ", password: "+ user['password'])
  }
  return false;
}
function password_is_valid(){
  for (let x in json) {
    if ($('#id_email').val() == json[x]['email']){
      return $('#id_password').val() == json[x]['password'];
    }
    // console.log(users_as_json[user]['email']);
    // console.log('email: ' + user['email'] + ", password: "+ user['password'])
  }
  // for (let i = 0; i<passwords.length; i++){
  //   // console.log(emails[i].innerHTML);
  //   if ($('#id_email').val() == emails[i].innerHTML){
  //     return $('#id_password').val() == passwords[i].innerHTML;
  //   }
  // }
}
$(document).ready(function () {
  $("#id_email").on('input', function (e) {
    if ($('#id_email').val() == ''){
      $('#email_message').text("Email tidak boleh kosong");
      $('#email_message').css({
        'z-index':'2',
        'color' : 'red',
      });
    }
    // email_is_valid()
    else if (email_is_valid()){
      $('#email_message').text("Email anda valid");
      $('#email_message').css({
        'z-index':'2',
        'color' : 'blue',
      });
    } else {
      $('#email_message').text("Maaf email anda belum terdaftar");
      $('#email_message').css({
        'z-index':'2',
        'color' : 'red',
      });
    }
    
  });
  $("button").on('click', function (e) {
    if ($('#id_password').val() == '') {
      e.preventDefault();
      $('#password_message').text("Password tidak boleh kosong");
      $('#password_message').css({
        'z-index':'2',
        'color' : 'red',
      });
    } else if (!password_is_valid()){
      e.preventDefault();
      $('#password_message').text("Maaf, password tidak sesuai");
      $('#password_message').css({
        'z-index':'2',
        'color' : 'red',
      });
      // console.log(e.target.value);
    }  
  });
  $('#id_password').focus(function (e) { 
    e.preventDefault();
    $('#password_message').css({
      'z-index':'-3',
      'color' : 'red',
    });
  });
});
