from django.shortcuts import render
from django.contrib.auth.models import User as DjangoUser
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .forms import EditProfileForm, ProfileForm 
from aplikasi_vaksin.models import User
from json import dumps

@login_required
def edit_profile(request):
    if request.method == 'POST':
        form = EditProfileForm(request.POST, instance=request.user)
        profile_form = ProfileForm(request.POST)
        users = User.objects.all().values()
        user_as_json = []
        for user in users:
            user_as_json.append({'email':user['email'], 'password': user['password']})

        if form.is_valid() and profile_form.is_valid():
            user_form = form.save()
            custom_form = profile_form.save(False)
            custom_form.user = user_form
            custom_form.save()
            context = {
            "forms": custom_form,
            'users' : users,
            'json': dumps(user_as_json),
            "icons" : [
            "user_profile/icon/email.png",
            "user_profile/icon/key.png",
            "user_profile/icon/person.png",
            "user_profile/icon/phone.png",
            "user_profile/icon/age.png",
            "user_profile/icon/id.png",
            "user_profile/icon/location.png",
            ]
        }
        if request.method == "POST":
            email = request.POST['email']
            password = request.POST['password']
            name = request.POST['name']
            user = DjangoUser.objects.create_user(email, email, password)
            user.first_name = name
            user.save()
            # redirect
            return redirect('view_profile.html')
            # return HttpResponseRedirect("/lab-4")
        return render(request, "edit_profile.html", context)
    else:
        form = EditProfileForm(instance=request.user)
        profile_form = ProfileForm(instance=request.user.userprofile)
        args = {}
        args['form'] = form
        args['profile_form'] = profile_form
        return render(request, 'edit_profile.html', args)

@login_required
def profile(request): 
    p = User.objects.filter(email=request.user.email,name=request.user.first_name)
    context = {
        'pengguna' : p[0]
    }
    return render(request, 'view_profile.html', context)



    # 'email' : User.objects.filter(User.email),
    # 'name' : User.objects.filter(User.name),
    # 'no_hp' : User.objects.filter(User.no_hp),
    # 'usia' : User.objects.filter(User.usia),
    # 'nik' : User.objects.filter(User.nik),
    # 'domisili' : User.objects.filter(User.domisili),