from django.http import response
from django.http.response import HttpResponseRedirect
from django.contrib.auth.models import User as DjangoUser
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect, render
from aplikasi_vaksin.models import User
from json import dumps

def user_login(request):
  users = User.objects.all().values()
  user_as_json = []
  for user in users:
    user_as_json.append({'email':user['email'], 'password': user['password']})
  response = {
    'users' : users,
    'json': dumps(user_as_json),
  }
  if request.method == "POST":
    # saat berhasil login, return ke home
    email = request.POST['email']
    password = request.POST['password']
    user = authenticate(request, username=email, password=password)
    if user is not None:
      login(request, user)
      return HttpResponseRedirect('/')
  return render(request, "user_login/index.html", response)

def user_logout(request):
  logout(request)
  return HttpResponseRedirect("/user/login")