from django.urls import path
from .views import  user_login, user_logout

urlpatterns = [
  # path('', index, name='index'),
  path('login/', user_login, name='user_login'),
  path('logout/', user_logout, name='user_logout'),
  # path('form/', add_note, name='add_list'),
]