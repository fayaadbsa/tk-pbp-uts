from django.urls import path

from . import views

urlpatterns = [
    path('add/', views.user_regis, name='user_regis'),
    path('update/', views.user_update_view, name='update_user'),
    path('ajax/load-tanggal/', views.load_tanggal, name='ajax_load_tanggal'), # AJAX
    path('ajax/load-jenis/', views.load_jenis, name='ajax_load_jenis')
]