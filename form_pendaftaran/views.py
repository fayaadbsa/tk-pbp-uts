from django.contrib.auth.models import User as DjangoUser
from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse,response
from django.contrib import messages
import json
from aplikasi_vaksin.models import User
from admin_tambah_faskes.models import Lokasi, TanggalWaktu, JenisVaksin
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required

from .forms import RegisForm
from aplikasi_vaksin.models import User
from json import dumps

# Create your views here.
def user_regis(request):
  # create object of form
  regis_form = RegisForm(request.POST or None, request.FILES or None)
  users = User.objects.all().values()
  user_as_json = []
  for user in users:
    user_as_json.append({'email':user['email']})
    
  # check if form data is valid
  if regis_form.is_valid():
    # save the form data to model
    regis_form.save()
  context = {
    "forms": regis_form,
    'users' : users,
    'json': dumps(user_as_json),
    "icons" : [
      "user_regis/icon/email.png",
      "user_regis/icon/key.png",
      "user_regis/icon/person.png",
      "user_regis/icon/phone.png",
      "user_regis/icon/age.png",
      "user_regis/icon/id.png",
      "user_regis/icon/location.png",
      "user_regis/icon/lokasi.png",
      ]
  }
  if request.method == "POST":
    email = request.POST['email']
    password = request.POST['password']
    name = request.POST['name']
    user = DjangoUser.objects.create_user(email, email, password)
    user.first_name = name
    user.save()
    # redirect
    return redirect('user_login')
    # return HttpResponseRedirect("/lab-4")
  return render(request, "user_regis/index.html", context)

def user_update_view(request, pk):
    daftar = get_object_or_404(Daftar, pk=pk)
    form = RegisForm(instance.initial(request.POST.copy()),instance=instance)
    if request.method == 'POST':
        form = RegisForm(request.POST, instance=daftar)
        if form.is_valid():
            form.save()
            return redirect('update_user', pk=pk)
    return render(request, 'user_regis/index.html', {'form': form})

# AJAX
def load_tanggal(request):
    lokasi_id = request.GET.get('lokasi')
    dates = TanggalWaktu.objects.filter(lokasi_id=lokasi_id).all()
    return render(request, 'user_regis/tanggal_dropdown_list_options.html', {'dates': dates})

def load_jenis(request):
    tanggal_id = request.GET.get('tanggal')
    types = JenisVaksin.objects.filter(tanggal_id=tanggal_id).order_by('name')
    return render(request, 'user_regis/jenis_dropdown_list_options.html', {'types': types})